"""Flask app normal routes."""
from flask import render_template, request, url_for, redirect, Blueprint
from flask_login import current_user, login_required, logout_user
from flask_sqlalchemy import SQLAlchemy
from .queues import file_creation

from . import login_manager
from .forms import VmForm
from .models import Vm, db

# Blueprint
views_bp = Blueprint(
    'views_bp',
    __name__,
    template_folder="templates",
    static_folder="static"
)


# Routes
@views_bp.route('/index/')
@login_required
def index():
    """Logged-in User Dashboard."""
    return render_template(
        "index.jinja2",
        title="AtheneApp.",
        template="dashboard-template",
        current_user=current_user,
        body="Vous êtes connecté !",
    )


@views_bp.route("/logout")
@login_required
def logout():
    """User log-out logic."""
    logout_user()
    return redirect(url_for("auth_bp.login"))


@views_bp.route('/formulaire/', methods=["GET", "POST"])
@login_required
def formulaire():
    # Page du formulaire
    form = VmForm()

    return render_template(
        "formulaire.jinja2",
        title="Formulaire",
        form=form,
        template="signup-page",
        body="Remplir le formulaire de création de VM."
    )


@views_bp.route('/formulaire/init/', methods=["GET", "POST"])
@login_required
def creation():
    # Traitement du Formulaire
    if request.method == 'POST':
        # Datas
        nom = request.form["name"]
        os = request.form["os"]
        stockage = request.form["stockage"]
        cpu_number = request.form["cpu"]
        # ram_memory = request.form["ram"]
        # netcard_number = request.form["netcard"]
        # groupe = request.form["groupe"]
        # hyperviseur = request.form["hyperviseur"]

        newVm = Vm(name=nom, memory=stockage, environment=os, vcpu=cpu_number)
        db.session.add(newVm)
        db.session.commit()

        # VM format JSON
        data = '{"nom":"'+nom+'", ' \
                '"groupe":"'+os+'", ' \
                '"memoire rom":"'+stockage+'", ' \
                '"nombre de CPU":"' + cpu_number + '"}'
                                                   # ',' \
                # '"memoire ram":"'+ram_memory+'",' \
                # '"nombre de carte réseau":"'+netcard_number+'",' \
                # '"environnement":"'+groupe+'",' \
                # '"hyperviseur":"'+hyperviseur+'"}'

        print(data)

        # file_creation.queue_creation(data)

        return data
