from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

db = SQLAlchemy()
login_manager = LoginManager()


def create_app():
    # Create app
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object("config.Config")

    # Initialise plugins
    db.init_app(app)
    login_manager.init_app(app)

    with app.app_context():
        from . import auth, views
        from .assets import compile_static_assets

        # Register Blueprints
        app.register_blueprint(auth.auth_bp)
        # Normal Blueprints
        app.register_blueprint(views.views_bp)

        # Create Database Models
        db.create_all()

        # Compile static assets
        # if app.config["FLASK_ENV"] == "development":
        #     compile_static_assets(app)

    return app
