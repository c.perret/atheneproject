import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='nommage')

channel.basic_publish(exchange='', routing_key='nommage', body='file nommage')
print("Naming VM")
connection.close()