import pika
from os import environ


def queue_creation(data):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='creation')

    channel.basic_publish(exchange='', routing_key='creation', body=data)
    print(" Creating VM'")
    connection.close()
