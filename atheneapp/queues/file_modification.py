import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='modification')

channel.basic_publish(exchange='', routing_key='modification', body='file modification')
print("Modification of a VM")
connection.close()