import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='general')

channel.basic_publish(exchange='', routing_key='general', body='file generale')
print(" Sending request")
connection.close()