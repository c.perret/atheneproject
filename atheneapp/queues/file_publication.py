import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='publication')

channel.basic_publish(exchange='', routing_key='publication', body='file publication')
print("Publishing VM")
connection.close()