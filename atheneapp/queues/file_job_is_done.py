import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='jobdone')

channel.basic_publish(exchange='', routing_key='jobdone', body='file job is done')
print("Request done")
connection.close()