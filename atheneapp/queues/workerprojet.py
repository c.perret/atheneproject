import pika
import sys
import os
import time
import json
from script_libvirt import createvm
import libvirt


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    listqueues = ['creation', 'jobdone', 'modification', 'nommage', 'publication', 'suppression']

    def callback(ch, method, properties, body):
        ch.basic_ack(delivery_tag=method.delivery_tag)
        print(method.routing_key)
        match method.routing_key:
            case "creation":
                #####################################################################
                # On crée une VM
                conn = None
                try:
                    conn = libvirt.open("qemu:///system")
                except libvirt.libvirtError as e:
                    print(repr(e), file=sys.stderr)
                    exit(1)

                dom = None
                try:
                    dom = conn.defineXMLFlags(createvm("test", "1572864", "2"), 0)
                except libvirt.libvirtError as e:
                    print(repr(e), file=sys.stderr)
                    exit(1)

                if dom.create() < 0:
                    print('Can not boot guest domain.', file=sys.stderr)
                    exit(1)

                print('Guest ' + dom.name() + ' has booted', file=sys.stderr)

                conn.close()
                exit(0)
                #####################################################################
            case "jobdone":
                print(" [x] Received %r" % body.decode())
                print(" [x] Done")
            case "modification":
                print(" [x] Received %r" % body.decode())
                print(" [x] Done")
            case "nommage":
                print(" [x] Received %r" % body.decode())
                print(" [x] Done")
            case "publication":
                print(" [x] Received %r" % body.decode())
                print(" [x] Done")
            case "suppression":
                print(" [x] Received %r" % body.decode())
                print(" [x] Done")

    # On consomme nos queues
    for queues in listqueues:
        print(queues)
        channel.queue_declare(queue=queues)
        channel.basic_consume(queue=queues, on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')

    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
