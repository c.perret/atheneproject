import pika

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='suppression')

channel.basic_publish(exchange='', routing_key='suppression', body='file suppression')
print("Deleting VM ")
connection.close()