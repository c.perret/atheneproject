"""Sign-up & log-in forms."""
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, SelectField, IntegerField
from wtforms.validators import DataRequired, Email, EqualTo, Length, Optional


class SignupForm(FlaskForm):
    """User Sign-up Form."""

    name = StringField("Name", validators=[DataRequired()])
    email = StringField(
        "Email",
        validators=[
            Length(min=6),
            Email(message="Entrez une adresse email valide."),
            DataRequired(),
        ],
    )
    password = PasswordField(
        "Password",
        validators=[
            DataRequired(),
            Length(min=6, message="Le mot de passe doit faire minimum 6 caractères."),
        ],
    )
    confirm = PasswordField(
        "Confirmez votre mot de passe",
        validators=[
            DataRequired(),
            EqualTo("password", message="Les mots de passe doivent correspondre."),
        ],
    )
    submit = SubmitField("Inscription")


class LoginForm(FlaskForm):
    """User Log-in Form."""

    email = StringField(
        "Email", validators=[DataRequired(), Email(message="Entrez une adresse email valide.")]
    )
    password = PasswordField("Mot de passe", validators=[DataRequired()])
    submit = SubmitField("Connexion")


class VmForm(FlaskForm):
    """Virtual machine Form"""

    name = StringField("Nom", validators=[DataRequired()])
    stockage = IntegerField("Stockage en KB", validators=[DataRequired()])
    cpu = IntegerField("Nombre de vCPU", validators=[DataRequired()])
    os = SelectField(
        "Choisissez un environnement",
        choices=[('fedora', 'Fedora'), ('linux', 'Rocky Linux 8'), ('debian', 'Debian 11')]
    )
    # ram = StringField("Mémoire RAM en KB", validators=[Optional()])
    # groupe = StringField("Groupe", validators=[Optional()])
    # netcard = StringField("Nombre de cartes réseau", validators=[Optional()])
    # hyperviseur = StringField("Hyperviseur de rattachement", validators=[Optional()])
    submit = SubmitField("Création")
