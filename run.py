#! /usr/bin/env python
from atheneapp import create_app
from os import environ
import subprocess

PATH_WORKER = environ.get("PATH_WORKER")
subprocess.Popen(['py', str(PATH_WORKER)])

app = create_app()


if __name__ == "__main__":
    app.run()
